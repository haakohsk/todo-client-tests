import * as React from 'react';
import { TaskDetails, TaskList, TaskNew, TaskEdit } from '../src/task-components';
import { shallow } from 'enzyme';
import { Form, Button, Column, Row, Card } from '../src/widgets';
import { NavLink } from 'react-router-dom';
import { createHashHistory } from 'history';

const history = createHashHistory();

jest.mock('../src/task-service', () => {
  class TaskService {
    getAll() {
      return Promise.resolve([
        { id: 1, title: 'Les leksjon', description: 'Les bror nøye', done: false },
        { id: 2, title: 'Møt opp på forelesning', description: 'mother', done: false },
        { id: 3, title: 'Gjør øving', description: 'KEKW', done: false },
      ]);
    }

    get() {
      return Promise.resolve({ id: 1, title: 'Les leksjon', description: 'Les bror nøye', done: false });
    }

    create() {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }

  }
  return new TaskService();
});


describe('Task component tests', () => {
  test('TaskList draws correctly', (done) => {
    const wrapper = shallow(<TaskList />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/tasks/1">Les leksjon</NavLink>,
          <NavLink to="/tasks/2">Møt opp på forelesning</NavLink>,
          <NavLink to="/tasks/3">Gjør øving</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  // tester at TaskDetails tegner riktig
  // funker ikke som tiltenkt
  test('TaskDetails draws correctly', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsMatchingElement([
          <Card title="Task">
            <Row>
              <Column>Les leksjon</Column>
            <Row>
          <Column>Les bror nøye</Column>
            </Row>
              <Form.Checkbox checked={true} onChange={() => {}} disabled />
            </Row>
            <Button.Success
          onClick={() => history.push('/tasks/')}>Edit</Button.Success>
          </Card>
          
        ])
      ).toEqual(true);
      done();
    });
  });

  // test med snapshot
  test('TaskDetails draws correctly with snapshot', (done) => {
    const wrapper = shallow(<TaskDetails match={{ params: { id: 1 } }} />);

    setTimeout(() => {
      expect(wrapper).toMatchSnapshot();
      done();
    });
  });


  test('TaskNew correctly sets location on create', (done) => {
    const wrapper = shallow(<TaskNew />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // @ts-ignore
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);

    wrapper.find(Form.Textarea).simulate('change', { currentTarget: { value: 'Kaffepausen varer i minst 50 min' } });
    // @ts-ignore
    expect(wrapper.containsMatchingElement(<Form.Textarea value="Kaffepausen varer i minst 50 min" />)).toEqual(true);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/tasks/4');
      done();
    });
  });

  test('TaskNew correctly sets location on edit', (done) => {
    const wrapper = shallow(<TaskEdit match={{ params: { id: 1 } }} />);

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // @ts-ignore
    expect(wrapper.containsMatchingElement(<Form.Input value="Kaffepause" />)).toEqual(true);

    wrapper.find(Form.Textarea).simulate('change', { currentTarget: { value: 'Kaffepause' } });
    // @ts-ignore
    expect(wrapper.containsMatchingElement(<Form.Textarea value="Kaffepause" />)).toEqual(true);
      
    done();
  });
});
